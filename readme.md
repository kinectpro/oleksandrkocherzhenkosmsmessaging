
<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>
<p align="center">

## Instructions for launching the project

</p>

<h5>1. Install Composer </h5>
composer install 
<h5>2. Install Laravel</h5>
composer global require "laravel/installer"
<h5>3. Update the Laravel framework</h5>
php composer.phar update 
<h5>4. Install PHP 7.2.9</h5>
http://www.linuxfromscratch.org/blfs/view/svn/general/php.html
<h5>5. Install PostgreSQL 10</h5>
sudo apt-get install postgresql-10
<h5>6. Create migration and seed</h5>
artisan migrate:refresh --seed 
<h5>7. Launch server </h5>
php artisan serve <br> 

## API
<h5>http://smsss.local/</h5>
<b>api/subscriber/{id}</b> - returns a subscriber by id<br>
<b>api/sms</b> - sending sms all subscribers (in laravel.log)<br>
<b>api/sms-new</b> - sending one sms (in laravel log)   


#
## Original documentation

https://laravel.com/docs/5.7/installation