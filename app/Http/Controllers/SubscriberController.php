<?php

namespace App\Http\Controllers;

use App\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Jobs\SendReminderSMS;
use Nutnet\LaravelSms\SmsSender;

class SubscriberController extends Controller
{
    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
//        return view('subscriber.profile', ['subscriber' => Subscriber::findOrFail($id)]);
        return Subscriber::findOrFail($id);
    }

//    public function sendSMS(Request $request, $id)
//    {
//        $subscriber = Subscriber::findOrFail($id);
//
//        $this->dispatch(new SendReminderSMS($subscriber));
//    }
    /**
     * @param SmsSender $smsSender
     */
    public function sendSms(SmsSender $smsSender)
    {
        // получаем подписчиков из БД
        $subscribers = Subscriber::all();

        // отправка сообщения подписчикам
        foreach ($subscribers as $subscriber) {
            $this->dispatch(new SendReminderSMS($subscriber->phone,"какой-то спам !!!"));
//            $smsSender->send($subscriber->phone, 'Текст сообщения');
        }
    }
}
