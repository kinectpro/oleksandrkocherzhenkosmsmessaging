<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    /**
     *
     * @var string
     */
    protected $table = 'subscriber';
}
