<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Nutnet\LaravelSms\SmsSender;

class SendReminderSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $sms;
    protected $phone;

    /**
     * SendReminderSMS constructor.
     * @param $number
     * @param $text
     */
    public function __construct($number, $text)
    {
        $this->phone = $number;
        $this->sms = $text;
    }

    /**
     * @param SmsSender $smsSender
     */
    public function handle(SmsSender $smsSender)
    {
        $smsSender->send($this->phone, $this->sms);
//        info($this->sms);
    }

    /**
     * @param \Exception $exception
     */
    public function failed(\Exception $exception)
    {
        // Отправляем пользователю уведомление об ошибке
        info($exception);
    }
}
