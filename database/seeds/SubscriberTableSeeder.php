<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubscriberTableSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            DB::table('subscriber')->insert([
                'id' => $i,
                'name' => 'Jon' . " " . $i,
                'phone' => '+' . random_int(10, 100000000000) . $i,
            ]);
        }
    }
}
